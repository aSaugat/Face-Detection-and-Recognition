Steps to run the project:

1. First run generator.py to generate the dataset.
2. Run trainer.py to train the model using the dataset you generated.
3. Run detector.py to detect and recognize the faces from the Video Feed.