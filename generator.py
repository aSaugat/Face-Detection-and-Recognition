__author__ = 'Saugat'

''' This file is used to generate the dataset from Video Feed by capturing images frame by frame.
    Images of each person in the dataset are assigned a unique label
 '''

import cv2
import os

#capture the video
cam = cv2.VideoCapture(0)

#using Haarcascades for detecting the faces
detector=cv2.CascadeClassifier(os.getcwd()+ '/haarcascades/haarcascade_frontalface_default.xml')

i=0
offset=50

#enter the id to assign labels to each person
name=input('enter your id')

while True:
    ret, im =cam.read()
    gray=cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
    faces=detector.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5, minSize=(100, 100), flags=cv2.CASCADE_SCALE_IMAGE)
    for(x,y,w,h) in faces:
        i=i+1
        cv2.imwrite("./Dataset/face-"+name +'.'+ str(i) + ".jpg", gray[y-offset:y+h+offset,x-offset:x+w+offset])
        cv2.rectangle(im,(x-50,y-50),(x+w+50,y+h+50),(225,0,0),2)
        cv2.imshow('im',im[y-offset:y+h+offset,x-offset:x+w+offset])
        cv2.waitKey(100)
    if i>20:
        cam.release()
        cv2.destroyAllWindows()
        break
