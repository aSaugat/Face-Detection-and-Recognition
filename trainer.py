__author__ = 'Saugat'

''' This file is used to train the model using the dataset we just generated'''

import cv2,os
import numpy as np
from PIL import Image

#create the model
model = cv2.face.LBPHFaceRecognizer_create()

cascadePath = os.getcwd()+ '/haarcascades/haarcascade_frontalface_default.xml'
faceCascade = cv2.CascadeClassifier(cascadePath)
path = './Dataset'

def get_images_and_labels(path):
     image_paths = [os.path.join(path, f) for f in os.listdir(path)]

     images = []
     labels = []

     for image_path in image_paths:

         image_pil = Image.open(image_path).convert('L')
         image = np.array(image_pil, 'uint8')

        #extract the labels from each images
         nbr = int(os.path.split(image_path)[1].split(".")[0].replace("face-", ""))
         print (nbr)

         faces = faceCascade.detectMultiScale(image)
         for (x, y, w, h) in faces:
             images.append(image[y: y + h, x: x + w])
             labels.append(nbr)
             cv2.imshow("Adding faces to traning set...", image[y: y + h, x: x + w])
             cv2.waitKey(10)
     # return the images list and labels list
     return images, labels


images, labels = get_images_and_labels(path)
cv2.imshow('test',images[0])
cv2.waitKey(1)

#training the models
model.train(images, np.array(labels))

#saving the trained model to a yml file which would be later used to for testing
model.save('./trainer/trainer.yml')

cv2.destroyAllWindows()


