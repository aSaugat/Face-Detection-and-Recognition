__author__ = 'Saugat'

''' This file is used to detect the face and then recognize whose face is being used as a test data'''

import cv2
import os

model = cv2.face.LBPHFaceRecognizer_create()

#loading the model we trained previously
model.read('./trainer/trainer.yml')

cascadePath = os.getcwd()+ '/haarcascades/haarcascade_frontalface_default.xml'
faceCascade = cv2.CascadeClassifier(cascadePath)
path = './Dataset'

cam = cv2.VideoCapture(0)

faces_tested = 0
faces_recognized = 0

while True:
    ret, im =cam.read()
    gray=cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)

    #detecting the faces
    faces=faceCascade.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5, minSize=(100, 100), flags=cv2.CASCADE_SCALE_IMAGE)
    for(x,y,w,h) in faces:
        nbr_predicted, conf = model.predict(gray[y:y+h,x:x+w])
        #printing Confidence score
        print(conf)
        cv2.rectangle(im,(x-50,y-50),(x+w+50,y+h+50),(225,0,0),2)
        if(conf <= 70.00):
            #recognizing the faces
            if(nbr_predicted==0):
                 nbr_predicted='Saugat'
            elif(nbr_predicted==1):
                 nbr_predicted='Bishal'
            elif(nbr_predicted==2):
                nbr_predicted='Utsab'
            elif(nbr_predicted==3):
                nbr_predicted='Rishi'
            elif(nbr_predicted==4):
                nbr_predicted='Arun'
            else:
                nbr_predicted='Face not Recognized'
        else:
            nbr_predicted = 'Not Detected'

        #display the recognized name of person and corresponding Confidence score on top of image
        cv2.putText(im, nbr_predicted, (x,y-20), cv2.FONT_HERSHEY_SIMPLEX, 1, 255, 2)
        cv2.putText(im,"Confidence: %s" % str(conf), (x-70,y-60),cv2.FONT_HERSHEY_SIMPLEX, 1, 255, 2 )

        cv2.imshow('Camera',im)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cam.release()
cv2.destroyAllWindows()

